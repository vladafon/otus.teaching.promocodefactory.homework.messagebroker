﻿namespace Otus.Teaching.Pcf.Contracts
{
    public class NotifyAdminAboutPartnerManagerPromoCodeContract
    {
        public Guid PartnerManagerId {  get; set; }
    }
}
